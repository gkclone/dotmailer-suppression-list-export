<?php

define ('ROOT', getcwd());
require ROOT . '/../vendor/autoload.php';

use Guzzle\Http\Client;
use Symfony\Component\Yaml\Parser;

out(date('jS F Y - H:i:s'), 'Script started');

//
// Load the main configuration file.
//
$config_dir = ROOT . '/config';

if (!file_exists($config_dir . '/config.yml')) {
  out('No config file found. Exiting...');
  exit(1);
}

$parser = new Parser();
$config = $parser->parse(file_get_contents($config_dir . '/config.yml'));

if (empty($config['endpoint'])) {
  out('No endpoint. Exiting...');
  exit(1);
}

if (empty($config['destination'])) {
  out('No destination. Exiting...');
  exit(1);
}

//
// Load the configuration file for each account.
//
if (!file_exists($config_dir . '/accounts.yml')) {
  out('No account config file found. Exiting...');
  exit(1);
}

out('Loading account config');
$account_configs = $parser->parse(file_get_contents($config_dir . '/accounts.yml'));

if (empty($account_configs)) {
  out('No account configuration found. Exiting...');
  exit(1);
}

out(count($account_configs) . ' accounts found...');
out(implode(' ', array_keys($account_configs)));

//
// Request data from dotMailer for each account.
//
$http_client = new Client();
$api_slug = '/contacts/suppressed-since/';
$select_size = 1000;

foreach ($account_configs as $account_config) {
  out($account_config['account'], 'Account name');

  if (!isset($account_config['outfile'])) {
    out('Skipping account - no outfile specified');
    continue;
  }

  out($account_config['created'], 'Suppressed since');

  // Setup the request for this account.
  $http_client->setDefaultOption('auth', array(
    $account_config['api']['username'], $account_config['api']['password'],
  ));

  $request = $http_client->get($config['endpoint'] . $api_slug . $account_config['created']);

  $query = $request->getQuery();
  $query->set('select', $select_size);

  // Open a temporary file for writing. We'll dump the CSV data here and move it
  // to it's final destination later if everything was successful.
  $tmp_filepath = '/tmp/ContactSuppressionCSVExport_tmp__' . $account_config['outfile'];

  if (!$file_handle = fopen($tmp_filepath, 'w')) {
    out('Failed to open temporary file for writing: ' . $tmp_filepath);
    continue;
  }

  // Write the header row.
  fputcsv($file_handle, contact_suppression_csv_export_headers());

  // The API only let's us retrieve 1000 records at a time. We'll have to make
  // multiple requests changing the offset each time until we get a response
  // containing less than 1000 records (meaning we've hit the end).
  $count = $iteration = 0;

  do {
    // Set the offset and make the request.
    $query->set('skip', $select_size * $iteration++);
    $response = $request->send();

    if ($response->isSuccessful()) {
      $raw_body = $response->getBody();
      $records = json_decode($raw_body);
      $count += count($records);

      // Write each record to the temporary file.
      foreach ($records as $record) {
        $suppressed_contact = $record->suppressedContact;

        fputcsv($file_handle, array(
          $suppressed_contact->email,
          $suppressed_contact->emailType,
          date('d/m/Y H:i', strtotime($record->dateRemoved)),
          $suppressed_contact->status,
        ));
      }
    }
  }
  while (count($records) == $select_size);

  fclose($file_handle);
  out($count, '# of contacts');

  // Move the temporary file to it's final destination.
  $destination_filepath = $config['destination'] . '/' . $account_config['outfile'] . '.csv';
  rename($tmp_filepath, $destination_filepath);
}

/**
 * Define the headers for the CSV export files.
 */
function contact_suppression_csv_export_headers() {
  return array(
    'Email',
    'EmailType',
    'DateRemoved',
    'Status',
  );
}

/**
 * Output on STDOUT.
 */
function out($out, $label = '') {
  fwrite(STDOUT, ($label ? "$label: " : '') . print_r($out, true) . "\n");
}
